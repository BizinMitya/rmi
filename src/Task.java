import java.io.Serializable;

/**
 * Created by Dmitriy on 23.03.2016.
 */
public class Task implements Serializable {
    public static final String ADDITION = "ADDITION";
    public static final String SUBTRACTION = "SUBTRACTION";
    public static final String MULTIPLICATION = "MULTIPLICATION";
    public static final String DIVISION = "DIVISION";

    private String operation;
    private float operands1;
    private float operands2;

    public Task(float operands1, float operands2, String operation) {
        this.operands1 = operands1;
        this.operands2 = operands2;
        this.setOperation(operation);
    }

    public float getOperands1() {
        return operands1;
    }

    public void setOperands1(float operands1) {
        this.operands1 = operands1;
    }

    public float getOperands2() {
        return operands2;
    }

    public void setOperands2(float operands2) {
        this.operands2 = operands2;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
