import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Dmitriy on 24.03.2016.
 */
public class CalculateImpl implements Calculate {

    public static void main(String[] args) {
        try {
            System.out.print("Starting registry...");
            Registry registry = LocateRegistry.createRegistry(2099);
            System.out.println(" OK");
            Calculate calculate = new CalculateImpl();
            Remote stub = UnicastRemoteObject.exportObject(calculate, 0);
            System.out.print("Binding service...");
            registry.bind("calculate", stub);
            System.out.println(" OK");
        } catch (RemoteException | AlreadyBoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    public float calculation(Task task) throws RemoteException {
        float result = 0;
        switch (task.getOperation()) {
            case "ADDITION": {
                result = task.getOperands1() + task.getOperands2();
                break;
            }
            case "SUBTRACTION": {
                result = task.getOperands1() - task.getOperands2();
                break;
            }
            case "MULTIPLICATION": {
                result = task.getOperands1() * task.getOperands2();
                break;
            }
            case "DIVISION": {
                if (task.getOperands2() != 0) {
                    result = task.getOperands1() / task.getOperands2();
                }
                break;
            }
        }
        return result;
    }
}
