import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Dmitriy on 23.03.2016.
 */
public interface Calculate extends Remote {
    float calculation(Task task) throws RemoteException;
}
