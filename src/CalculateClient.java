import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

/**
 * Created by Dmitriy on 23.03.2016.
 */
public class CalculateClient {

    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            Registry registry = LocateRegistry.getRegistry("localhost", 2099);
            Calculate calculate = (Calculate) registry.lookup("calculate");
            float op1, op2;
            String operation = "";
            label:
            while (true) {
                System.out.println("Введите название операции или любое слово для выхода:");
                operation = scanner.next();
                switch (operation) {
                    case "ADDITION": {
                        break;
                    }
                    case "SUBTRACTION": {
                        break;
                    }
                    case "MULTIPLICATION": {
                        break;
                    }
                    case "DIVISION": {
                        break;
                    }
                    default: {
                        break label;
                    }
                }
                System.out.println("Введите первый операнд:");
                op1 = Float.parseFloat(scanner.next());
                System.out.println("Введите второй операнд:");
                op2 = Float.parseFloat(scanner.next());
                System.out.println("Результат операции " + operation + ": " + calculate.calculation(new Task(op1, op2, operation)));
            }
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }
}

